use strict;
use warnings;

my @sports=('football', 'cricket', 'Tennis');
my $sports_ref=\@sports;
&mysub($sports_ref);

print "$sports_ref\n";  #This will give reference value
print "@$sports_ref\n"; #This will give dereference value

sub mysub{
	my $ref=$_[0];
	my @mysports=@$ref;
	print $mysports[1],  "\n";
}

