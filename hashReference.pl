use strict;
use warnings;

my %hash=(m=>1,n=>2,o=>3);
my $hashref=\%hash;
my $hashref1={x=>11,y=>12,z=>13};


print "$hashref\n";   #This will give reference value
print "$hashref->{m}\n"; #This will give dereference value
print "$hashref1->{y}\n";#This will give dereference value
mysub($hashref);


sub mysub{
		my $ref=$_[0];
		print $$ref{o},  "\n";
		print ${$_[0]}{n},  "\n";


	}


