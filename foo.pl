use foo;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(bar blat);

foo::bar( "a" );
foo::blat( "b" );
