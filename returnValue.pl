sub get_available_flavors {
    return ("chocolate chip", "oatmeal raisin", "peanut butter");
}

my @flavors = get_available_flavors();
print "@flavors\n";
